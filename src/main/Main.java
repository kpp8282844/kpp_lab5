package main;
import my_classes.MenuManager;
import my_classes.Organiser;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Organiser organiser = Organiser.getInstance();
        Scanner scanner = new Scanner(System.in);

        boolean exit = false;
        int userChoice;

        while (!exit) {
            MenuManager.displayMenu();

            try {
                userChoice = scanner.nextInt();
                scanner.nextLine();

                switch (userChoice) {
                    case 1:
                        MenuManager.invokeSecondWordsMethod(organiser);
                        break;
                    case 2:
                       MenuManager.invokeWordsByLengthInQuestions(organiser);
                        break;
                    case 3:
                        exit = true;
                        break;
                    default:
                        System.out.println("Invalid choice. Please enter a number between 1 and 3.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a number.");
                scanner.nextLine();
            }
        }

        System.out.println("Bye bye darling!");
    }
}