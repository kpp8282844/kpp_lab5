package my_classes;

import java.util.*;
import java.util.ArrayList;
import java.util.List;

public class Organiser {
    private Organiser() {
    }
    private static Organiser instance;

    public static Organiser getInstance() {
        if (instance == null) {
            instance = new Organiser();
        }
        return instance;
    }

    public List<String> getSecondWordsListInText(String text, String startLetter) {
        List<String> secondWordsList = new ArrayList<>();
        if (text.isEmpty()) {
            return secondWordsList;
        }

        if (TextAnalyst.isOneWord(text)) {
            return secondWordsList;
        }

        List<String> sentences = TextAnalyst.splitIntoSentences(text);

        for (var sent : sentences) {
            String secondWord = TextAnalyst.extractSecondWordFromSentence(sent);
            if (secondWord.startsWith(startLetter)) {
                secondWordsList.add(secondWord);
            }
        }

        return secondWordsList;
    }

    public Set<String>  getWordsByLengthInQuestions(String text, int wordLength){
        Set<String> wordsListOfLength = new LinkedHashSet<>();

        if (text.isEmpty()) {
            return wordsListOfLength;
        }

        List<String> sentences = TextAnalyst.splitIntoSentences(text);

        for (var sent : sentences) {
            if(TextAnalyst.isInterrogativeSentence(sent)){
                List<String> allWords = TextAnalyst.getWordsFromSentence(sent);
                wordsListOfLength.addAll(allWords);
                wordsListOfLength.removeIf(word -> word.length() != wordLength);
            }
        }

        return wordsListOfLength;
    }

}
