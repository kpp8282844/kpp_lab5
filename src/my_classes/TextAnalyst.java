package my_classes;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class TextAnalyst {
    public static boolean isOneWord(String input) {
        return input.trim().matches("[\\p{P}\\s]*[^\\p{P}\\s]+[\\p{P}\\s]*");
    }

    public static List<String> splitIntoSentences(String text) {
        List<String> sentences = new ArrayList<>();

        if (text.isEmpty())
            return sentences;

        //sentence ending regex
        String sentenceRegex = "[.!?]+\\s*";

        Pattern pattern = Pattern.compile(sentenceRegex);

        Matcher matcher = pattern.matcher(text);

        //splitting into sentences
        int startIndex = 0;
        while (matcher.find()) {
            int endIndex = matcher.end();
            String sentence = text.substring(startIndex, endIndex).trim();
            sentences.add(sentence);
            startIndex = endIndex;
        }

        //adding the remaining part of the text as the last sentence
        if (startIndex < text.length()) {
            String lastSentence = text.substring(startIndex).trim();
            sentences.add(lastSentence);
        }

        return sentences;
    }

    public static String extractSecondWordFromSentence(String sentence) {
        String wordRegex = "\\w+";

        Pattern pattern = Pattern.compile(wordRegex);

        Matcher matcher = pattern.matcher(sentence);

        int wordCount = 0;
        while (matcher.find()) {
            wordCount++;
            if (wordCount == 2) {
                return matcher.group();
            }
        }

        //If there is no second word return an empty string
        return "";
    }

    public static boolean isInterrogativeSentence(String sentence){
        String regex =  ".+[?]+[!?.\\s]*";
        return sentence.trim().matches(regex);
    }

    public static List<String> getWordsFromSentence(String sentence){
        List<String> words = new ArrayList<>();

        if (sentence.isEmpty()){
            return words;
        }

        Pattern pattern = Pattern.compile("\\w+");
        Matcher matcher = pattern.matcher(sentence);

        while (matcher.find()){
            words.add(matcher.group());
        }

        return words;
    }
}
