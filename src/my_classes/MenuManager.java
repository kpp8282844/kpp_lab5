package my_classes;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

public abstract class MenuManager {
    public static void displayMenu(){
        System.out.println("Choose desired action:");
        System.out.println("1. Get second words starting with preferred pattern");
        System.out.println("2. Get words of indicated length from interrogative questions");
        System.out.println("3. Flee away");
    }

    public static void invokeSecondWordsMethod(Organiser organiser) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the text: ");
        String text = scanner.nextLine();

        System.out.print("Enter the start letter(s): ");
        String startLetter = scanner.nextLine();

        List<String> result = organiser.getSecondWordsListInText(text, startLetter);

        if (result.isEmpty())
            System.out.println("There is nothing to show you...");
        else {
            System.out.println("Second words starting with '" + startLetter + "':");
            for (String word : result) {
                System.out.println(word);
            }
        }
    }

    public static void invokeWordsByLengthInQuestions(Organiser organiser) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the text: ");
        String text = scanner.nextLine();

        System.out.print("Enter the word length: ");
        int wordLength = scanner.nextInt();

        Set<String> result = organiser.getWordsByLengthInQuestions(text, wordLength);

        if (result.isEmpty()) {
            System.out.println("No words of length " + wordLength + " in interrogative sentences or no such sentences.");
        } else {
            System.out.println("Words of length " + wordLength + " in interrogative sentences: " + result);
        }
    }

}
