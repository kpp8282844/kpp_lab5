package test;
import my_classes.TextAnalyst;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;


class TextAnalystTest {
    public static List<String> getOneWordSentences(){
        List<String> sentences = new ArrayList<>();

        sentences.add("[People)?");
        sentences.add("   ROmanchik!!!! ");
        sentences.add("bork..........  ");
        return sentences;
    }
    @Disabled
    @ParameterizedTest
    @MethodSource("getOneWordSentences")
    void testOneWordSentenceDefining(String oneWordSentence){
           assertTrue(TextAnalyst.isOneWord(oneWordSentence),
                   () -> "Pattern doesn't match sentence: " + oneWordSentence
           );
    }

    public static List<String> getMultipleWordSentences(){
        List<String> sentences = new ArrayList<>();

        sentences.add("[People) jfjf?");
        sentences.add("1   ROmanchik!!!! ");
        sentences.add("bork .  4 ");
        return sentences;
    }

    @Disabled
    @ParameterizedTest
    @MethodSource("getMultipleWordSentences")
    void testMultipleWordSentenceDefining(String multiWordSentence){
        assertFalse(TextAnalyst.isOneWord(multiWordSentence),
                () -> "Pattern surprisingly match sentence: " + multiWordSentence
        );
    }

    public static List<String> getInterrogativeSentences() {
        return Arrays.asList(
                "Is a interrogative sentence?",
                "What???!!!!",
                "EEE boy my boy ....!!!???? ?? ",
                "1??"
        );
    }
    @Disabled
    @ParameterizedTest
    @MethodSource("getInterrogativeSentences")
    void testIsInterrogativeSentenceTrue(String interrogativeSentence) {
        assertTrue(TextAnalyst.isInterrogativeSentence(interrogativeSentence),
                () -> "Expected true for interrogative sentence: " + interrogativeSentence
        );
    }
    public static List<String> getNonInterrogativeSentences() {
        return Arrays.asList(
                "This is a sentence.....",
                "She likes ? me bra!!!!.",
                "The"
        );
    }

    @Disabled
    @ParameterizedTest
    @MethodSource("getNonInterrogativeSentences")
    void testIsInterrogativeSentenceFalse(String nonInterrogativeSentence) {
        assertFalse(TextAnalyst.isInterrogativeSentence(nonInterrogativeSentence),
                () -> "Expected false for non-interrogative sentence: " + nonInterrogativeSentence
        );
    }

    @Disabled
    @Test
    void testSplitIntoSentences() {
        String text = "This is sentence one. Sentence two bra!!!. And sentence three ee boy!?";

        List<String> expectedSentences = List.of(
                "This is sentence one.",
                "Sentence two bra!!!.",
                "And sentence three ee boy!?"
        );
        List<String> actualSentences = TextAnalyst.splitIntoSentences(text);
        assertEquals(expectedSentences, actualSentences,
                () -> "Splitting into sentences failed...");
    }

    @Disabled
    @Test
    void testSplitIntoSentencesWithEmptyText() {
        String emptyText = "";
        List<String> result = TextAnalyst.splitIntoSentences(emptyText);
        assertEquals(List.of(), result, "Empty text must result in empty List...");
    }

    @Disabled
    @Test
    void testSplitIntoSentencesWithNoSentenceEndingCharacters() {
        String textWithoutSentences = "This is a non punctuation text blabal";

        List<String> result = TextAnalyst.splitIntoSentences(textWithoutSentences);

        assertEquals(List.of(textWithoutSentences), result,
                "Text without punctuation is kind of shithousery and must be as 1 sentence");
    }

    @Disabled
    @Test
    void testExtractSecondWordFromSentence() {
        String sentence = "This is a Politechtttt.";

        String expectedSecondWord = "is";
        String actualSecondWord = TextAnalyst.extractSecondWordFromSentence(sentence);

        assertEquals(expectedSecondWord, actualSecondWord,
                () -> "Failed to extract planned words");
    }


    @Disabled
    @Test
    void testExtractSecondWordFromSentenceWithEmptySentence() {
        String emptySentence = "";

        String result = TextAnalyst.extractSecondWordFromSentence(emptySentence);

        assertEquals("", result, "Empty sentence smust be an empty string.");
    }
    @Disabled
    @Test
    void testExtractSecondWordFromSentenceWithSingleWordSentence() {
        String singleWordSentence = "Hello!!!!!!!!!";
        String result = TextAnalyst.extractSecondWordFromSentence(singleWordSentence);
        assertEquals("", result, "Single word sentence must produce empty result list");
    }
}